class IssueReportsController < ApplicationController
  unloadable

  before_filter :find_project_by_project_id
  before_filter :authorize

  def index; end

  def create
    @results = RedmineIssueReport.summary(
      @project.id,
      params[:issue_reports_start_date],
      params[:issue_reports_due_date],
      params[:issue_reports_tracker]
    )

    @resolved_issues = issues_list('due_date', true)
    @resolved_details = RedmineIssueReport.details(@resolved_issues)

    @unresolved_issues = issues_list('start_date', false)
    @unresolved_details = RedmineIssueReport.details(@unresolved_issues)

    @config = RedmineIssueReportConfig.new

    render :index
  end

  private

  def issues_list(field, is_closed)
    RedmineIssueReport.list(
      field,
      is_closed,
      @project.id,
      params[:issue_reports_start_date],
      params[:issue_reports_due_date],
      params[:issue_reports_tracker]
    )
  end
end
