Redmine::Plugin.register :redmine_issue_reports do
  name 'Redmine Issue Reports plugin'
  author 'Luiz Sanches'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'https://gitlab.com/nomadetec/redmine-issue-reports'
  author_url 'https://gitlab.com/nomadetec'

  project_module :issue_reports do
    permission :index_reports, issue_reports: :index
  end

  permission :issue_reports, { issue_reports: [:create] }, public: true

  menu :project_menu,
       :issue_reports,
       { controller: 'issue_reports', action: 'index' },
       param: :project_id,
       before: :settings
end
